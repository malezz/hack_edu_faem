import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueResource from 'vue-resource'
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

Vue.config.productionTip = false;
Vue.use(VueResource)
Vue.http.options.root = 'http://172.31.20.70:1324'

new Vue({
  router,
  store,
  vuetify,
  iconfont: 'mdi',
  render: h => h(App)
}).$mount("#app");
