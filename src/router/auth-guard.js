// import store from '../store'
export default function (to, from, next) {
  // if (store.getters.user) { по хорошему надо проверять
  if (localStorage.login) {
    next()
  } else {
    next('/login')
  }
}
