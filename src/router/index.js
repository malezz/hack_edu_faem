import Vue from "vue"
import AuthGuard from './auth-guard'
import VueRouter from "vue-router"
import Auth from "../views/Auth.vue"
import TrackMap from "../views/TrackMap.vue"
import Project from "../views/Project.vue"
import Landing from "../views/Landing.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Auth
  },
  {
    path: "/",
    name: "Landing",
    component: Landing
  },
  {
    path: "/map",
    name: "Map",
    component: TrackMap,
    beforeEnter: AuthGuard
  },
  {
    name: 'Project',
    path: '/project/:id',
    component: Project,
    props: true,
    beforeEnter: AuthGuard
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
